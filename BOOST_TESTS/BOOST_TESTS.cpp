#define BOOST_TEST_MAIN Bfx.Alex.Configuration.Manager_Test 

#include <boost/test/test_tools.hpp>
#include <boost/test/unit_test.hpp>

#include "Plugins.h"

BOOST_AUTO_TEST_CASE(AnalyzeConfigFileTest)
{
	 Plugins plugin;

     // Alex JSON Validator
	 {	
		BOOST_TEST( plugin.jsonValidator( plugin.readJsonFile(AlexConfig), plugin.readJsonFile(SchemaFile) ) == true ); 
	 }

	 // Leeres Config File -> Fehler! 
	 {
		std::string configFileContent = "";
		BOOST_TEST( *plugin.ConfigFileContentOutput( configFileContent ) == error );
	 }

 	 // Leeres JSON Object -> Fehler!
	 {
		 std::string configFileContent = "{}";	
		 BOOST_TEST( *plugin.ConfigFileContentOutput( configFileContent ) == error );
	 }

	 // 1 gültiges Config File!				
	 {
		 std::string configFileContent = R"({ "modelPlugins": { "1": "Bfx.Alex.Entitlement.Model.dll" } })";
		 BOOST_TEST( !plugin.ConfigFileContentOutput( configFileContent ) );
	 }

	 // 1 gültiges Config File!				
	 {
		 std::string configFileContent = R"({ "modelPlugins" : { "1" : "Bfx.Alex.Entitlement.Model.dll", "2" : "Bfx.Alex.Computation.Austria.Model.dll" } })";
		 BOOST_TEST( !plugin.ConfigFileContentOutput( configFileContent ) );
	 }

	 // 1 gültiges Config File?	// Gültig, weil doppelter Index ignoriert wird!
	 {
		 std::string configFileContent = R"({ "modelPlugins" : { "1" : "Bfx.Alex.Entitlement.Model.dll", "1" : "Bfx.Alex.Computation.Austria.Model.dll" } })";
		 BOOST_TEST( *plugin.ConfigFileContentOutput( configFileContent ) == error );
	 }

	 // falscher key							
	 {
		 std::string configFileContent = R"({ "modelPlugiXX" : { "1" : "Bfx.Alex.Entitlement.Model.dll", "2" : "Bfx.Alex.Computation.Austria.Model.dll" } })";
		 BOOST_TEST( *plugin.ConfigFileContentOutput( configFileContent ) == error );
	 }

	 // dopplepunkt fehlt
	 {
		 std::string configFileContent = R"({ "modelPlugins" { "1" : "Bfx.Alex.Entitlement.Model.dll" } })";
		 BOOST_TEST( *plugin.ConfigFileContentOutput( configFileContent ) == error );		
	 }

	 // 2 Fehler im Value
	 {
		 std::string configFileContent = R"({ "modelPlugins" : { "1" : "Bfx.Alex.Entitlement.Modelxxx.dll" } })";
		 BOOST_TEST( *plugin.ConfigFileContentOutput( configFileContent ) == error );
	 }

	 // .dll fehlt
	 {
/*		 std::string configFileContent = R"({ "modelPlugins" : { "1" : "Bfx.Alex.Entitlement.Model" } })";
		 BOOST_TEST( *ConfigFileContentOutput( configFileContent ) == warning );*/		
	 }

	 // Model fehlt
	 {
		 std::string configFileContent = R"({ "modelPlugins" : { "1" : "Bfx.Alex.Entitlement.dll" } })";
		 BOOST_TEST( *plugin.ConfigFileContentOutput( configFileContent ) == error );		
	 }

	 // Kein modelPlugin
	 {
		 std::string configFileContent = R"({ "modelPlugins" : { "1" : "Bfx.Alex.PermissionWorkflow.UI.Architecture.dll" } })";
		 BOOST_TEST( *plugin.ConfigFileContentOutput( configFileContent ) == error );		
	 }

	 // Kein uiPlugin
	 {
		 std::string configFileContent = R"({ "uiPlugins" : { "1" : "Bfx.Alex.AttendanceRecorder.Interface.Model.dll" } })";
		 BOOST_TEST( *plugin.ConfigFileContentOutput( configFileContent ) == error );		
	 }

	 // Kein webuiPlugins
	 {
		 std::string configFileContent = R"({ "webuiPlugins" : { "1" : "Bfx.Alex.AttendanceRecorder.Interface.Model.dll" } })";
		 BOOST_TEST( *plugin.ConfigFileContentOutput( configFileContent ) == error );		
	 }
}
