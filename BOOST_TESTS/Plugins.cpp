#include "Plugins.h"

using namespace boost::dll;
using namespace boost::filesystem;

namespace pt = boost::property_tree;

json Plugins::readJsonFile( std::string_view file )
{
    if( std::filesystem::exists(file) )
    {
        std::ifstream ifs( file.data() );

        if( ifs.is_open() )
        {
            json jsonFile = json::parse(ifs );
           
            return jsonFile;
        }
    }
	else
	{
		std::cerr << file << " not found!" << std::endl;
	}

    return {};
}

bool Plugins::jsonValidator( const nlohmann::basic_json<> jsonConfig, const nlohmann::basic_json<> jsonSchema )
{
    json_validator validator;

    try {
         validator.set_root_schema( jsonSchema );
    } 
    catch ( const std::exception &e ) 
    {
        std::cerr << "Validation of schema failed, here is why: " << e.what() << "\n";
        return false;
    }

    for ( auto &alexJson : { jsonConfig } ) {
       
        try {
            validator.validate( alexJson );
            return true;
        } 
        catch ( const std::exception &e ) 
        {
            std::cerr << e.what() << std::endl;
            return false;
        }
    }
}

bool Plugins::findUIModel( std::string functionName, Set modelList, std::set<std::string> ignore )
{
	if( !ignore.contains(functionName ) )
	{
		boost::replace_all(functionName, "UI", "Model" );
		
		if( std::find(modelList.begin(), modelList.end(), functionName) != modelList.end() )
			return true;
		else 
			return false; 			 			
	}
}

Plugins::Plugins()
{	
    if( fileLists.empty() )
	{
		for( auto& file : std::filesystem::directory_iterator( std::filesystem::current_path().string() ) )
		{
		    if( file.path().extension() == ".dll" )
		        fileLists.insert( fileLists.begin(), file.path().filename().string() );                   
		}   
    }
}

void Plugins::LoadConfigFileSchema()
{
	pt::ptree tree;

	if( std::filesystem::exists( SchemaFile ) )
	{
		pt::read_json( SchemaFile, tree );
	
		for( auto& data : tree.get_child( "configFileSchema" ) )
		{	
			modelLists.insert( data.first );
            auto ptree = tree.get_child( "configFileSchema." + data.first );
        
			if( !ptree.empty() )
            {
				for( auto& configFileSchemaName : tree.get_child("configFileSchema." + data.first) )
					functionName.insert( { data.first, configFileSchemaName.second.data() } );
			}
    
		}
	}
	else
	{
		std::cerr << "File " << SchemaFile << " not found!" << std::endl;
	}
	
}

OptionalSeverity Plugins::ConfigFileContentOutput( std::string_view fileContent )
{	
	pt::ptree tree;
	std::stringstream configFile( fileContent.data() );
	
	if( modelLists.empty() )
		LoadConfigFileSchema();

	try
	{
		pt::read_json( configFile, tree );
		
		if( tree.empty() ) 
			return error;
		
	    for( const auto& [ key, data ] : tree.get_child("") )
		{			
			if( !modelLists.contains( key ) ) 
				return error;
			
			if( key.find( "Plugins" ) != std::string::npos )
			{	
				for( const auto& [ childKey, childData ] : tree.get_child( key ) )
				{												
					if( !findPlugin(key, childData.data()) )
						return error;
					if( childData.data().find(".dll") == std::string::npos ) 
						return error;	
					if( tree.get_child(key).count(childKey) >= 2 ) 
						return error;
					if( childData.data().empty() )
						continue;
				}
			}
		}
		return {};
	}
	catch( pt::json_parser::json_parser_error& er )
	{
		// std::cerr << er.what();
		return error;
	}
}

OptionalSeverity Plugins::ConfigFileOutput( const std::string& fileName )
{
	std::ifstream file( fileName, std::ios::binary );
    std::string fileContent;

	if( file.is_open() )
	{
		copy( std::istreambuf_iterator<char>( file ),
			  std::istreambuf_iterator<char>(),
			  std::back_insert_iterator<std::string>( fileContent ) );
	}
	else
		return error;
		
	return ConfigFileContentOutput( fileContent );
}

OptionalSeverity Plugins::ConfigFilesOutput( const std::string& directory )
{
	return {};
}

Set Plugins::FindModel( const std::string& function ) const
{
    std::set<std::string> list;

    for( auto& fileName : fileLists )
    {
        if( BoostHasFunction(function, fileName ) )
            list.insert(fileName);
    }

    return list;   
}
 
bool Plugins::findPlugin( const std::string& key, const std::string& name )
{	
	if( pluginsLists.empty() )
		for(const auto [key, value] : functionName )	
			pluginsLists.insert( { key, FindModel(value) } );
	
	return pluginsLists[key].contains(name);
}

bool Plugins::BoostHasFunction( std::string_view function, std::string_view fileName ) const
{	
	if( !std::filesystem::exists( std::filesystem::current_path().string() + "\\" + fileName.data() ) )
		return false;
		
		try {
			boost::dll::shared_library lib( fileName.data(), boost::dll::load_mode::dont_resolve_dll_references );

			if( !lib.has( function.data() ) )
				return false;
			else
				return true;		

		}catch( boost::dll::fs::error_code& e )
		{
			std::cerr << e.what() << std::endl;
		}

}


bool Plugins::HasFunction( std::string_view function, std::string_view fileName ) const
{
	std::string path = std::filesystem::current_path().string() + "\\" + fileName.data();

	if( !std::filesystem::exists( path ) )
		return false;

	if (std::filesystem::exists( path ))
	{	
		auto dll = ::LoadLibrary( path.c_str() );
	
		if (dll != NULL)
		{
			auto addr = ::GetProcAddress( dll, function.data() );
		
			if (!addr)
				return false;
			else
				return true;
		}		

	}

}
