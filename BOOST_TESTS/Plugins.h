#pragma once
#include <set>
#include <list>
#include <map>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <Windows.h>

#include <boost/dll/import.hpp>
#include <boost/dll/shared_library.hpp> 
#include <boost/dll.hpp>
#include <boost/function.hpp>

#include <boost/json.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <nlohmann/json-schema.hpp>

#include <boost/algorithm/string/replace.hpp>;

#include "Config.h"

using nlohmann::json;
using nlohmann::json_schema::json_validator;

using Set = std::set<std::string>;

using Severity = std::string;
using OptionalSeverity = std::optional< Severity >;

const Severity error = "error";
const Severity warning = "warning";

class Plugins
{
public:
	Plugins();
	
	OptionalSeverity ConfigFileContentOutput( std::string_view fileContent );
	bool jsonValidator( const nlohmann::basic_json<> jsonConfig, const nlohmann::basic_json<> jsonSchema );
	json readJsonFile( std::string_view file );
	bool findUIModel( std::string functionName, Set modelList, std::set<std::string> ignore );
	std::map< std::string, std::set<std::string> > pluginsLists;

private:

	Set FindModel( const std::string& key ) const;

	OptionalSeverity ConfigFileOutput( const std::string& fileName );
	OptionalSeverity ConfigFilesOutput( const std::string& directory );

	void LoadConfigFileSchema();

	bool findPlugin( const std::string& key, const std::string& name );
	bool HasFunction( std::string_view function, std::string_view fileName ) const;
	bool BoostHasFunction( std::string_view function, std::string_view fileName ) const;

protected:

	std::map< std::string, std::string > functionName;
	std::list< std::string > fileLists;
	std::set< std::string > modelLists;

};

