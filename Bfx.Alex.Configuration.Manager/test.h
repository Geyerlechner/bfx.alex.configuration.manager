#pragma once
#include <filesystem>
#include <string_view>

bool is_file_exist( std::string_view fileName ) 
{	
	const std::filesystem::path p = fileName ;
    return ( std::filesystem::exists(p) );
}

