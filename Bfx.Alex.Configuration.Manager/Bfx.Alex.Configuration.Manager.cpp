#include <iostream>
#include "../BOOST_TESTS/Plugins.h"

#include <boost/algorithm/string/replace.hpp>;


json readJsonFile( std::string_view file )
{
    if( std::filesystem::exists(file) )
    {
        std::ifstream ifs( file.data() );

        if( ifs.is_open() )
        {
            json jsonFile = json::parse(ifs );
           
            return jsonFile;
        }
    }
	else
	{
		std::cerr << file << " not found!" << std::endl;
	}

    return {};
}

const json Schema = R"(
{
  "properties": {
    
    "db": {   
            "type": "object",
            "required": ["connectionInfo"],
            "properties": {
                "connectionInfo": {"type": "string"}
            }
    },

    "modelServer": {
           "type": "object",
           "required": ["host", "port"],
           "properties": {
                   "host": {
                            "type": "string",
                            "anyOf": [
                                { "pattern": "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\\/(3[0-2]|[1-2][0-9]|[0-9]))?$" }
                                
                            ]
                            
                            
                    },
                   "port": {"type": "string", "minLength": 2, "maxLength": 5}
           },

    "country": {
      "default": "United States of America",
      "enum": ["United States of America", "Canada"]
    }

    },
    
    "httpServer":                 {},
    "webServer":                  {},
    "certificate":                {},
    "debug":                      {},
    "service":                    {},
    "freeze":                     {},
    "adresses":                   {},
    "modelServerConfiguration":   {},
    "S3":                         {},
    "computationAustriaModel":    {}
    
}, "required": [ "db", "modelServer"],
    "type": "object",

  "configFileSchema": {
      "modelPlugins":           { "functionName":     "GetBitFactoryModelPlugin"          },
      "uiPlugins":              { "functionName":     "GetBitFactoryUIPlugin"             },
      "architectureuiPlugins":  { "functionName":     "GetBitFactoryArchitectureUIPlugin" },
      "webuiPlugins":           { "functionName":     "GetBitFactoryWebUIPlugin"          },
      "httpServicePlugins":     { "functionName":     "GetBitFactoryHTTPServicePlugin"    },
      "serverDaemonPlugins":    { "functionName":     "GetBitFactoryServerPlugin"         },
      "tcxFilesystem":          {},
      "windowsDefaultPage":     {}
    }
  }

)"_json;

const json SchemaTest = R"(
{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "additionalProperties": false,
    "description": "Describes the 4-tuple of IP Addresses and/or Ports to be matched or translated to.",
    "minProperties": 1,
    "properties": {

        "modelServer": {
                        "type": "object",
                        "required": ["ip"],
                        "properties": {
                        "ip": {
                            "type": "string",
                            "anyOf" : [
                                    { "format": "ipv4" },
                                    { "format": "ipv6" }
                                ]   
                            }
                    }            
                    },

        "destination_ip": {
            "anyOf": [
                {
                    "format": "ipv4"
                },
                {
                    "format": "ipv6"
                }
            ],
            "description": "Destination IP address",
            "type": "string"
        },
        "destination_port": {
            "description": "Destination port",
            "maximum": 65535,
            "minimum": 1,
            "type": "integer"
        },
        "source_ip": {
            "anyOf": [
                {
                    "format": "ipv4"
                },
                {
                    "format": "ipv6"
                }
            ],
            "description": "Sender IP address",
            "type": "string"
        },
        "source_port": {
            "description": "Sender port",
            "maximum": 65535,
            "minimum": 1,
            "type": "integer"
        }
    },
    "title": "Network address translation 4-tuple",
    "type": "object"
}
)"_json;





bool jsonValidator( const nlohmann::basic_json<> jsonConfig, const nlohmann::basic_json<> jsonSchema )
{
    json_validator validator;

    try {
         validator.set_root_schema( jsonSchema );
    } 
    catch ( const std::exception &e ) 
    {
        std::cerr << "Validation of schema failed, here is why: " << e.what() << "\n";
        return false;
    }

    for ( auto &alexJson : { jsonConfig } ) {
         // std::cout << "About to validate this person:\n" << std::setw(2) << alexJson << std::endl;
        try {

            validator.validate( alexJson );
            std::cout << "Validation succeeded\n";
        } 
        catch ( const std::exception &e ) 
        {
            std::cerr << e.what() << std::endl;
            return false;
        }
    }

    return true;
}

std::vector<std::string> modelLists = {
"Bfx.Alex.AccountsReport.DayInterval.Model.dll", 
"Bfx.Alex.AttendanceRecorder.AutoBreak.Model.dll",
"Bfx.Alex.AttendanceRecorder.AutoMark.Model.dll",
"Bfx.Alex.AttendanceRecorder.Interface.Model.dll",
"Bfx.Alex.AttendanceRecorder.Model.dll",
"Bfx.Alex.AttendanceRecorder.StandBySWOeBAGS.Model.dll",
"Bfx.Alex.Automatic.Accounting.Model.dll",
"Bfx.Alex.CaretimeEntitlement.Model.dll",
"Bfx.Alex.ClassicImport.Model.dll",
"Bfx.Alex.Computation.Austria.CheckIdlePeriod.Model.dll",
"Bfx.Alex.Computation.Austria.Gunskirchen.Model.dll",
"Bfx.Alex.Computation.Austria.LienzBKH.Model.dll",
"Bfx.Alex.Computation.Austria.MONDI.AMS.Model.dll",
"Bfx.Alex.Computation.Austria.MONDI.Model.dll",
"Bfx.Alex.Computation.Austria.Medic.Model.dll",
"Bfx.Alex.Computation.Austria.Model.dll",
"Bfx.Alex.Computation.Austria.OEAMTC.Model.dll",
"Bfx.Alex.Computation.Austria.OEAMTC.Wien.Model.dll",
"Bfx.Alex.Computation.Austria.PKAKV.Model.dll",
"Bfx.Alex.Computation.Austria.PremiQaMed.Model.dll",
"Bfx.Alex.Computation.Austria.VGK.Model.dll",
"Bfx.Alex.Computation.Education.Model.dll",
"Bfx.Alex.Computation.NOE.Doc.Model.dll",
"Bfx.Alex.Customization.KUK.Model.dll",
"Bfx.Alex.EmployeeAttendanceInfo.Model.dll",
"Bfx.Alex.EmployeeCalendar.Model.dll",
"Bfx.Alex.EmployeeCardID.Model.dll",
"Bfx.Alex.EmployeeEmailAddress.Model.dll",
"Bfx.Alex.EmployeeImport.Model.dll",
"Bfx.Alex.EmployeeInfo.Model.dll",
"Bfx.Alex.EmployeePlanSymbolFromTo.Model.dll",
"Bfx.Alex.Entitlement.Model.dll",
"Bfx.Alex.ExportToSAPBI.Model.dll",
"Bfx.Alex.ManningReport.Model.dll",
"Bfx.Alex.MaternityLeave.Model.dll",
"Bfx.Alex.Model.AKHLinzWebService.dll",
"Bfx.Alex.MonthReport.Model.dll",
"Bfx.Alex.Outfitting.Model.dll",
//"Bfx.Alex.PermissionWorkflow.Model.dll",
"Bfx.Alex.PlanSymbolsToExchange.Model.dll",
"Bfx.Alex.Qualification.Accounts.Model.dll",
"Bfx.Alex.Qualification.Management.Model.dll",
"Bfx.Alex.RequestHoliday.Model.dll",
"Bfx.Alex.Service.Management.Model.dll",
"Bfx.Alex.ShiftExtension.Model.dll",
"Bfx.Alex.SmartTimeSheet.Model.dll",
"Bfx.Alex.TimeStampLog.Model.dll",
"Bfx.Alex.VacationFormMail.Model.dll",
"Bfx.Alex.WageAccountingAutomatic.Model.dll",
"Bfx.Alex.Zeitgeist.Model.dll",
"Bfx.Alex.Zeitgeist.TidyUp.Model.dll"
};

std::vector< std::string > uiPlugins = {
//"Bfx.Alex.AttendanceRecorder.Interface.Standalone.UI.dll",
"Bfx.Alex.AttendanceRecorder.Interface.UI.dll",
"Bfx.Alex.AttendanceRecorder.UI.dll",
"Bfx.Alex.ClassicImport.UI.dll",
"Bfx.Alex.EmployeeImport.UI.dll",
"Bfx.Alex.ManningReport.UI.dll",
"Bfx.Alex.PermissionWorkflow.UI.dll",
//"Bfx.Alex.QA.UI.dll",
"Bfx.Alex.WageAccountingAutomatic.UI.dll",
"Bfx.Alex.Zeitgeist.UI.dll"
};

bool hasModelFunction( std::vector<std::string>& modelList, std::string functionName )
{
    boost::replace_all(functionName, "UI", "Model");

    if( std::find(modelList.begin(), modelList.end(), functionName) != modelList.end() )
        return true;
    else 
        return false;
}

int main( int argc, char* argv[] ) 
{
    //for(auto&e : uiPlugins)
    //{
    //    if( hasModelFunction(modelLists, e) )
    //        std::cout << "[X] " << e << std::endl;
    //    else
    //        std::cout << "[ ] " << e << std::endl;
    // }

    std::jthread t([]{
        for (auto i = 0; i < 100; i++)
        {
            std::cout << i << std::endl;
        }
    });
  //t.joinable();
    std::cout << "A" << std::endl;

    //for(auto&e : uiPlugins)
    //{
    //    boost::replace_all(e, "UI", "Model");
    //    
    //    if( std::find(modelLists.begin(), modelLists.end(), e) != modelLists.end() )
    //    {
    //        std::cout << "[X]: " << e << std::endl;
    //    }
    //    else
    //        std::cout << "[ ]: " << e << std::endl;
    //}


    //if( jsonValidator(AlexConfig, SchemaTest) )
    //    std::cout << "JSON OK!" << std::endl;
    //else
    //    std::cerr << "JSON ERROR" << std::endl;

}

